﻿using Microsoft.EntityFrameworkCore;
using PolicyManagementAPI.Model;
using System.Collections.Generic;

namespace PolicyManagementAPI.Context
{
    public class UserDbcontext : DbContext
    {
        public UserDbcontext(DbContextOptions<UserDbcontext> Context) : base(Context)
        {

        }
        public DbSet<User> Userstbl { get; set; }
        public DbSet<Policies> Policytbl { get; set; }
        public DbSet<UserRegisteredForPolicy> userRegisteredForPoliciestbl { get; set; }
        public DbSet<UserSubscribedPolicy> userSubscribedPolicytbl { get; set; }
        public DbSet<UserRatingClass> userratingtbl { get; set; }
    }
}
