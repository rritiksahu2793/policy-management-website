﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PolicyManagementAPI.Model;
using PolicyManagementAPI.Services;
using System.Diagnostics.Contracts;

namespace PolicyManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        readonly IAdminService _IAdminService;
        public AdminController(IAdminService adminService)
        {
            _IAdminService = adminService;
        }
        #region AdminGetAllPendingPolicy
        [Route("GetAllPendingPolicy")]
        [HttpGet]
        public ActionResult AdminGetAllPendingPolicy()
        {
            List<UserRegisteredForPolicy> AllPendingPolicy = _IAdminService.GetAllPendingPolicy();
            return Ok(AllPendingPolicy);
        }
        #endregion

        #region AdminApprovepolicy
        [Route("AdminApprovePolicy")]
        [HttpPost]
        public ActionResult AdminApprovedPolicy(UserAndPolicy UandP)
        {
            bool res = _IAdminService.ApprovePolicy(UandP);
            return Ok(res);
        }
        #endregion

        #region AdminRejectPolicy

        [Route("AdminRejectPolicy")]
        [HttpPost]
        public ActionResult AdminRejectPolicy(UserAndPolicy UandP , string reasonForCancellation)
        {
            bool res = _IAdminService.RejectPolicy(UandP , reasonForCancellation);
            return Ok(res);
        }
        #endregion
        [Route("GetAllReviews")]
        [HttpGet]
        public ActionResult GetAllReviews()
        {
            List<UserRatingClass> AllReviews = _IAdminService.GetAllReviews();
            return Ok(AllReviews);

        }
    }
}
