﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using PolicyManagementAPI.Model;
using PolicyManagementAPI.Services;

namespace PolicyManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PolicyController : ControllerBase
    {
        IPolicyService _IPolicyService;
        public PolicyController(IPolicyService policyService)
        {
            _IPolicyService = policyService;
        }

        public static int PolicyId;

        #region GetAllPolicy
        [HttpGet]
        public ActionResult GetAllPolicy()
        {
            List<Policies> allpolicy = _IPolicyService.GetAllPolicy();
            return Ok(allpolicy);
        }
        #endregion

        #region GetPolicyDetails
        [Route("GetPolicyDetails/{policyId:int}")]
        [HttpGet]
        public ActionResult GetPolicyDetails(int policyId)
        {
            Policies policy = _IPolicyService.GetPolicyDetails(policyId);
            return Ok(policy);
        }
        #endregion

        #region AddPolicy
        [Route("AddPolicy")]
        [HttpPost]
        public ActionResult AddPolicy(Policies Policy)
        {
            bool addPolicyStatus = _IPolicyService.AddPolicy(Policy);
            return Created("api/Created", addPolicyStatus);
        }
        #endregion

        #region DeletePolicy
        [Route("DeletePolicy/{id:int}")]
        [HttpDelete]
        public ActionResult DeletePolicy(int id)
        {
            bool deletePolicyStatus = _IPolicyService.DeletePolicy(id);
            return Ok(deletePolicyStatus);
        }
        #endregion

        #region UpdatePolicy
        [Route("UpdatePolicy{id:int}")]
        [HttpPost]
        public ActionResult UpdatePolicy(int id, Policies policy)
        {
            policy.policyId = id;
            if (_IPolicyService.UpdatePolicy(policy))
            {
                return Created("api/Updated", true);
            }
            else
            {
                return BadRequest("Policy not found");
            }
        }
        #endregion










    }
}
