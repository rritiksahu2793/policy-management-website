﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PolicyManagementAPI.Model;
using PolicyManagementAPI.Services;
using PolicyManagementAPI.SRPLogic;

namespace PolicyManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ExceptionHandler]
    public class UserController : ControllerBase
    {

        readonly IUserService _IuserService;
        readonly IPolicyService _IPolicyService;
        readonly ITokenService _ITokenService;
        public UserController(IUserService userService , IPolicyService policyService, ITokenService iTokenService)
        {
            _IuserService = userService;
            _IPolicyService = policyService;
            _ITokenService = iTokenService;
        }
        public static int LogedUserId;

        #region GetAllUsers
        [HttpGet]
        public ActionResult GetAllUser()
        {
            List<User> allUsers = _IuserService.GetAllUser();
            return Ok(allUsers);
        }
        #endregion

        #region AddUser
        [Route("AddUser")]
        [HttpPost]
        public ActionResult AddUser(User user)
        {
            bool addUserStatus = _IuserService.AddUser(user);
            return Created("api/Created", addUserStatus);
        }
        #endregion

        #region DeleteUser
        [Route("DeleteUser/{id:int}")]
        [HttpDelete]
        public ActionResult DeletePolicy(int id)
        {
            bool deleteUserStatus = _IuserService.DeleteUser(id);
            return Ok(deleteUserStatus);
        }
        #endregion

        #region UpdateUser
        [Route("UpdateUser{id:int}")]
        [HttpPost]
        public ActionResult UpdateUser(int id, User user)
        {
            user.id = id;
            if (_IuserService.UpdateUser(user))
            {
                return Created("api/Updated",true);
            }
            else
            {
                return BadRequest("User not found");
            }
        }
        #endregion

        #region UserRegisteredForPolicy
        [Route("UserRegisterForPolicy")]
        [HttpPost]
        public ActionResult UserRegisterForPolicy( UserAndPolicy UandP) // not implemented in repository to it afer migration 
        {
            User user = _IuserService.GetUserById(UandP.userId);
            Policies policy = _IPolicyService.GetPolicyById(UandP.policyId);
            UserRegisteredForPolicy userRegisteredForPolicy = new UserRegisteredForPolicy();
            userRegisteredForPolicy.policyId = policy.policyId;
            userRegisteredForPolicy.policyName = policy.policyName;
            userRegisteredForPolicy.userName = user.name;
            userRegisteredForPolicy.UserEmail = user.email;
            userRegisteredForPolicy.status = "Pending";
            userRegisteredForPolicy.reasonForCancellation = "";
            userRegisteredForPolicy.userId = user.id;
            bool response = _IuserService.RegisterPolicyForUser(userRegisteredForPolicy);
            return Ok(response);
        }
        #endregion

        #region LoginUser
        [Route("LoginUser")]
        [HttpPost]
        public ActionResult Login(LoginClass LoginUser)
        {
            User user = _IuserService.Login(LoginUser);
            LogedUserId = user.id;
            string token = _ITokenService.GenerateJSONWebToken(user);
            var Token = JsonConvert.SerializeObject(new {TOKEN = token , Name=user.name,Id=user.id,UserEmail = user.email ,Password=user.password});
            return Created("api/Login Success", Token );

        }
        #endregion

        #region getUserById
        [Route("GetUserById/{userId}")]
        [HttpGet]
        public ActionResult GetUserByIdl(string userId)
        {
            int Id = int.Parse(userId);
            User user = _IuserService.GetUserById(Id);
            return Ok(user);
        }
        #endregion

        #region getUserSubscribedpolicy
        [Route("GetUserSubscribedPolicy/{userId}")]
        [HttpGet]
        public ActionResult GetUserSubscribedPolicy(string userId)
        {
            int UserId = int.Parse(userId);
            List<UserSubscribedPolicy> AllSubscribedPolicy = _IuserService.GetUserSubscribedPolicy(UserId);
            return Ok(AllSubscribedPolicy);
        }
        #endregion

        #region GetUserPendingpolicy
        [Route("GetUserPendingPolicy/{userId}")]
        [HttpGet]
        public ActionResult GetUserPendingPolicy(string userId)
        {
            int UserId = int.Parse(userId);
            List<UserRegisteredForPolicy> AllPendingPolicy = _IuserService.GetUserPendingPolicy(UserId);
            return Ok(AllPendingPolicy);
        }
        #endregion

        #region GetUserRecejtedPolicy
        [Route("GetUserRejectedPolicy/{userId}")]
        [HttpGet]
        public ActionResult GetUserRejectedPolicy(string userId)
        {
            int UserId = int.Parse(userId);
            List<UserRegisteredForPolicy> AllRejectedPolicy = _IuserService.GetUserRejectedPolicy(UserId);
            return Ok(AllRejectedPolicy);
        }
        #endregion

        #region PayforPolicy

        [Route("PayForPolicy")]
        [HttpPost]
        public ActionResult PayForPolicy(PaymentClass payForPolicy)
        {
            bool paymentStatus = _IuserService.PayForSubscribedPolicy(payForPolicy);
            return Ok(paymentStatus);
        }

        #endregion

        #region UserRating
        [Route("UserRateforWebSite")]
        [HttpPost]
        public ActionResult userRating(UserRatingClass userrating)
        {
            bool res = _IuserService.UserRatingForWebsite(userrating);
            return Ok(res);

        }
        #endregion
    }
}
