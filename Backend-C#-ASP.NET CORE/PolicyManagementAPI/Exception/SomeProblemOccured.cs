﻿namespace PolicyManagementAPI.Exception
{
    public class SomeProblemOccured:ApplicationException
    {
        public SomeProblemOccured()
        {

        }
        public SomeProblemOccured(string msg):base(msg)
        {

        }
    }
}
