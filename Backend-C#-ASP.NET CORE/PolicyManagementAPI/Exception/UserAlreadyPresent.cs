﻿namespace PolicyManagementAPI.Exception
{
    public class UserAlreadyPresent:ApplicationException
    {
        public UserAlreadyPresent()
        {

        }
        public UserAlreadyPresent(string msg):base(msg)
        {

        }
    }
}
