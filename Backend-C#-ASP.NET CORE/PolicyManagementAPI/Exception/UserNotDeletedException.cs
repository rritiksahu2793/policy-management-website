﻿namespace PolicyManagementAPI.Exception
{
    public class UserNotDeletedException:ApplicationException
    {
        public UserNotDeletedException()
        {

        }
        public UserNotDeletedException(string msg):base(msg)
        {

        }
    }
}
