﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PolicyManagementAPI.Migrations
{
    public partial class first : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Policytbl",
                columns: table => new
                {
                    policyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    policyDiscription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    policyName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    policyCategory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    policyImagePath = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    totalCost = table.Column<int>(type: "int", nullable: false),
                    timeDuration = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Policytbl", x => x.policyId);
                });

            migrationBuilder.CreateTable(
                name: "Userstbl",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    city = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    age = table.Column<int>(type: "int", nullable: false),
                    aadharNo = table.Column<long>(type: "bigint", nullable: false),
                    panNo = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    salary = table.Column<int>(type: "int", nullable: false),
                    state = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Mobile = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Userstbl", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Policytbl");

            migrationBuilder.DropTable(
                name: "Userstbl");
        }
    }
}
