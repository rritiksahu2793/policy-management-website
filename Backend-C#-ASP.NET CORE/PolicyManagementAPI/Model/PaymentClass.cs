﻿namespace PolicyManagementAPI.Model
{
    public class PaymentClass
    {
        public int userId { get; set; }
        public int policyId { get; set; }
        public int amount { get; set; }
    }
}
