﻿using System.ComponentModel.DataAnnotations;

namespace PolicyManagementAPI.Model
{
    public class Policies
    {
        [Key]
        public int policyId { get; set; }
        public string policyDiscription { get; set; }
        public string policyName { get; set; }
        public string policyCategory { get; set; }
        public string policyImagePath { get; set; }
        public int totalCost { get; set; }
        public int timeDuration { get; set; }
    }
}
