﻿using System.ComponentModel.DataAnnotations;

namespace PolicyManagementAPI.Model
{
    public class User
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }

        //[DataType(DataType.Password)]
        public string password { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public int age { get; set; }
        public long aadharNo { get; set; }
        public string panNo { get; set; }
        public int salary { get; set; }
        public string state { get; set; }
        public long Mobile { get; set; }
    }
}
