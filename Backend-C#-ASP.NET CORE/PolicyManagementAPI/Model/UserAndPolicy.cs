﻿namespace PolicyManagementAPI.Model
{
    public class UserAndPolicy
    {
        public int userId { get; set; }
        public int policyId { get; set; }
    }
}
