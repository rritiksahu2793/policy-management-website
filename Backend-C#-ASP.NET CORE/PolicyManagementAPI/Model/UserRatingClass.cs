﻿namespace PolicyManagementAPI.Model
{
    public class UserRatingClass
    {
        public int id { get; set; }
        public string userName { get; set; }
        public int rating { get; set; }
        public string comment { get; set; }
    }
}
