﻿using System.ComponentModel.DataAnnotations;

namespace PolicyManagementAPI.Model
{
    public class UserRegisteredForPolicy
    {
        [Key]
        public int id { get; set; }
        public int userId { get; set; }
        public string UserEmail { get; set; }
        public string userName { get; set; }
        public int policyId { get; set; }
        public string policyName { get; set; }
        public string status { get; set; }
        public string reasonForCancellation { get; set; }
    }
}
