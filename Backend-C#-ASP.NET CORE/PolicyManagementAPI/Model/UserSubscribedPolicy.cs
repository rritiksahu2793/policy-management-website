﻿namespace PolicyManagementAPI.Model
{
    public class UserSubscribedPolicy
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string policyName { get; set; }
        public int policyId { get; set; }
        public int userId { get; set; }
        public int amountLeftToPay { get; set; }
    }
}
