﻿using PolicyManagementAPI.Context;
using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Repository
{
    public class AdminRepository : IAdminRepository
    {
        readonly UserDbcontext _userDbContext;
        readonly IPolicyRepository _policyRepository;
        public AdminRepository(UserDbcontext userDbContext , IPolicyRepository policyRepository)
        {
            _userDbContext = userDbContext;
            _policyRepository = policyRepository;
        }

        public bool ApprovePolicy(UserAndPolicy uandP)
        {
            UserRegisteredForPolicy registerdPolicy = _userDbContext.userRegisteredForPoliciestbl.Where(u=> u.policyId==uandP.policyId && u.userId==uandP.userId).FirstOrDefault();
            UserSubscribedPolicy userSubscribedPolicy = new UserSubscribedPolicy();
            userSubscribedPolicy.policyId = registerdPolicy.policyId;
            userSubscribedPolicy.userId = registerdPolicy.userId;
            userSubscribedPolicy.policyName = registerdPolicy.policyName;
            userSubscribedPolicy.userName = registerdPolicy.userName;
            userSubscribedPolicy.amountLeftToPay = _policyRepository.GetPolicyById(uandP.policyId).totalCost;

            _userDbContext.userSubscribedPolicytbl.Add(userSubscribedPolicy);
            _userDbContext.SaveChanges();

            _userDbContext.userRegisteredForPoliciestbl.Remove(registerdPolicy);
            _userDbContext.SaveChanges();

            return true;
        }

        public List<UserRegisteredForPolicy> getAllPendingpolicy()
        {
           return _userDbContext.userRegisteredForPoliciestbl.Where(u => u.status == "Pending").ToList();
        }

        public List<UserRatingClass> getAllReviews()
        {
            return _userDbContext.userratingtbl.ToList();
        }

        public bool RejectPolicy(UserAndPolicy uandP , string reasonForCancellation)
        {
            Console.WriteLine($"Policy ID :{uandP.policyId}");
            Console.WriteLine($"User ID :{uandP.userId}");
            Console.WriteLine(reasonForCancellation);
            foreach (UserRegisteredForPolicy u in _userDbContext.userRegisteredForPoliciestbl)
            {
                if (u.policyId == uandP.policyId && u.userId==uandP.userId)
                {
                    u.reasonForCancellation = reasonForCancellation;
                    u.status = "Rejected";
                }
            }
            _userDbContext.SaveChanges();
            return true;
            
        }
    }
}
