﻿using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Repository
{
    public interface IAdminRepository
    {
        bool ApprovePolicy(UserAndPolicy uandP);
        List<UserRegisteredForPolicy> getAllPendingpolicy();
        List<UserRatingClass> getAllReviews();
        bool RejectPolicy(UserAndPolicy uandP, string reasonForCancellation);
    }
}
