﻿using Microsoft.AspNetCore.JsonPatch;
using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Repository
{
    public interface IPolicyRepository
    {
        void AddPolicy(Policies policy);
        void DeletePolicyById(int id);
        List<Policies> GetAllPolicy();
        Policies GetPolicyById(int policyId);
        void UpdatePolicy(Policies policy);
    }
}
