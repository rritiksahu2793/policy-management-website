﻿using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Repository
{
    public interface IUserRepository
    {
        void AddUser(User user);
        void DeleteUser(int id);
        List<User> GetAllUsers();
        User GetUserByEmail(string email);
        User GetUserById(int id);
        List<UserRegisteredForPolicy> GetUserPendingPolicy(int userId);
        List<UserRegisteredForPolicy> GetUserRejectedPolicy(int userId);
        List<UserSubscribedPolicy> GetUserSubscribedPolicy(int userId);
        User Login(LoginClass loginUser);
        bool PayForSubscribedPolicy(PaymentClass payForPolicy);
        bool RegisterPolicyForUser(UserRegisteredForPolicy userRegisteredForPolicy);
        void UpdateUser(User user);
        bool UserRatingForWebSite(UserRatingClass userrating);
    }
}
