﻿using Microsoft.AspNetCore.JsonPatch;
using PolicyManagementAPI.Context;
using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Repository
{
    public class PolicyRepository:IPolicyRepository
    {
        UserDbcontext _userDbContext;
        public PolicyRepository(UserDbcontext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        public void AddPolicy(Policies policy)
        {
            _userDbContext.Policytbl.Add(policy);
            _userDbContext.SaveChanges();
        }

        public void DeletePolicyById(int id)
        {
            _userDbContext.Policytbl.Remove(GetPolicyById(id));
            _userDbContext.SaveChanges();
        }

        public List<Policies> GetAllPolicy()
        {
            return _userDbContext.Policytbl.ToList();
        }

        public Policies GetPolicyById(int policyId)
        {
            return _userDbContext.Policytbl.Where(p => p.policyId == policyId).FirstOrDefault();
        }

        public void UpdatePolicy(Policies policy)
        {
            _userDbContext.Policytbl.Remove(_userDbContext.Policytbl.Where(x => x.policyId == policy.policyId).FirstOrDefault());
            _userDbContext.Policytbl.Add(policy);
            _userDbContext.SaveChanges();
        }

        //public bool UpdatePolicy(int id, JsonPatchDocument policy)
        //{
        //    Policies policyToUpdate = GetPolicyById(id);
        //    if (policyToUpdate != null)
        //    {
        //        policy.ApplyTo(policyToUpdate);
        //        return true;
        //    }

        //    return false;
        //}


    }
}

