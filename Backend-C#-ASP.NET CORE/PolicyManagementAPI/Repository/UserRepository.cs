﻿using PolicyManagementAPI.Context;
using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Repository
{
    public class UserRepository : IUserRepository
    {
        UserDbcontext _userDbContext;
        public UserRepository(UserDbcontext userDbContext)
        {
            _userDbContext = userDbContext;
        }
        public void AddUser(User user)
        {
            _userDbContext.Userstbl.Add(user);
            _userDbContext.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            _userDbContext.Userstbl.Remove(GetUserById(id));
            _userDbContext.SaveChanges();
        }

        public List<User> GetAllUsers()
        {
            return _userDbContext.Userstbl.ToList();
        }

        public User GetUserByEmail(string email)
        {
            return _userDbContext.Userstbl.Where(u => u.email == email).FirstOrDefault();
        }

        public User GetUserById(int id)
        {
            return _userDbContext.Userstbl.Where(u => u.id == id).FirstOrDefault();
        }

        public List<UserRegisteredForPolicy> GetUserPendingPolicy(int userId)
        {
            return _userDbContext.userRegisteredForPoliciestbl.Where(u => u.userId == userId && u.status == "Pending").ToList();
        }

        public List<UserRegisteredForPolicy> GetUserRejectedPolicy(int userId)
        {
            return _userDbContext.userRegisteredForPoliciestbl.Where(u => u.userId == userId && u.status == "Rejected").ToList();
        }

        public List<UserSubscribedPolicy> GetUserSubscribedPolicy(int userId)
        {
            return _userDbContext.userSubscribedPolicytbl.Where(u=>u.userId==userId).ToList();
        }

        public User Login(LoginClass loginUser)
        {
            return _userDbContext.Userstbl.Where(u => u.email == loginUser.email && u.password == loginUser.password).FirstOrDefault();
        }

        public bool PayForSubscribedPolicy(PaymentClass payForPolicy)
        {
            foreach (UserSubscribedPolicy u in _userDbContext.userSubscribedPolicytbl)
            {
                if (u.policyId == payForPolicy.policyId && u.userId == payForPolicy.userId)
                {
                    u.amountLeftToPay = u.amountLeftToPay - payForPolicy.amount;
                    
                }
            }
            _userDbContext.SaveChanges();
            return true;

        }

        public bool RegisterPolicyForUser(UserRegisteredForPolicy userRegisteredForPolicy)
        {
            _userDbContext.userRegisteredForPoliciestbl.Add(userRegisteredForPolicy);
            _userDbContext.SaveChanges();
            return true;
        }

        public void UpdateUser(User user)
        {
            _userDbContext.Userstbl.Remove(_userDbContext.Userstbl.Where(x => x.id == user.id).FirstOrDefault());
            _userDbContext.Userstbl.Add(user);
            _userDbContext.SaveChanges();

        }

        public bool UserRatingForWebSite(UserRatingClass userrating)
        {
            bool userfound = false;
            foreach (UserRatingClass u in _userDbContext.userratingtbl)
            {
                if (u.userName == userrating.userName)
                {
                    u.rating = userrating.rating;
                    u.comment = userrating.comment;
                    userfound = true;
                }
            }
            if (userfound)
            {
                _userDbContext.SaveChanges();
                return true;
            }
            else
            {
                _userDbContext.userratingtbl.Add(userrating);
                _userDbContext.SaveChanges();
                return true;
            }
            
        }
    }
}
