﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PolicyManagementAPI.Exception;

namespace PolicyManagementAPI.SRPLogic
{
    public class ExceptionHandlerAttribute:ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(UserCredentialInvalidException))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            else if (context.Exception.GetType() == typeof(UserAlreadyPresent))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            else if (context.Exception.GetType() == typeof(UserNotDeletedException))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            else
            {
                context.Result = new StatusCodeResult(500);
            }
        }
    }
}
