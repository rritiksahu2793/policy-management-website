﻿using PolicyManagementAPI.Exception;
using PolicyManagementAPI.Model;
using PolicyManagementAPI.Repository;

namespace PolicyManagementAPI.Services
{
    public class AdminService : IAdminService
    {
        readonly IAdminRepository _adminRepository;
        public AdminService(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        public bool ApprovePolicy(UserAndPolicy uandP)
        {
           bool res = _adminRepository.ApprovePolicy(uandP);
            if (res)
            {
                return true;
            }
            else
            {
                throw new SomeProblemOccured("Opps Some Problem Occured ! Try again");
            }
        }

        public List<UserRegisteredForPolicy> GetAllPendingPolicy()
        {
            return _adminRepository.getAllPendingpolicy();
        }

        public List<UserRatingClass> GetAllReviews()
        {
            return _adminRepository.getAllReviews();
        }

        public bool RejectPolicy(UserAndPolicy uandP , string reasonForCanclelation)
        {
            bool res = _adminRepository.RejectPolicy(uandP , reasonForCanclelation);
            if (res)
            {
                return true;
            }
            else
            {
                throw new SomeProblemOccured("Opps Some Problem Occured ! Try again");
            }
        }
    }
}
