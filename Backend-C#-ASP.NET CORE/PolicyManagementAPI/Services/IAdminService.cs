﻿using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Services
{
    public interface IAdminService
    {
        bool ApprovePolicy(UserAndPolicy uandP);
        List<UserRegisteredForPolicy> GetAllPendingPolicy();
        List<UserRatingClass> GetAllReviews();
        bool RejectPolicy(UserAndPolicy uandP , string reasonForCanclelation);
    }
}
