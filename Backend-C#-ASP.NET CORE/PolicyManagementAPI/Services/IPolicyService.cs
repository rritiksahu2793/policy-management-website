﻿using Microsoft.AspNetCore.JsonPatch;
using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Services
{
    public interface IPolicyService
    {
        bool AddPolicy(Policies policy);
        bool DeletePolicy(int id);
        List<Policies> GetAllPolicy();

        Policies GetPolicyById(int id);
        Policies GetPolicyDetails(int policyId);
        bool UpdatePolicy(Policies policy);
    }
}
