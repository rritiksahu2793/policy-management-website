﻿using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Services
{
    public interface ITokenService
    {
       string GenerateJSONWebToken(User user);
    }
}
