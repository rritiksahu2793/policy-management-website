﻿using PolicyManagementAPI.Model;

namespace PolicyManagementAPI.Services
{
    public interface IUserService
    {
        bool AddUser(User user);
        bool DeleteUser(int id);
        List<User> GetAllUser();
        bool UpdateUser(User user);
        User GetUserById(int id);
        bool RegisterPolicyForUser(UserRegisteredForPolicy userRegisteredForPolicy);
        User Login(LoginClass loginUser);
        User GetUserByEmail(string email);
        List<UserRegisteredForPolicy> GetUserPendingPolicy(int userId);
        List<UserRegisteredForPolicy> GetUserRejectedPolicy(int userId);
        List<UserSubscribedPolicy> GetUserSubscribedPolicy(int userId);
        bool PayForSubscribedPolicy(PaymentClass payForPolicy);
        bool UserRatingForWebsite(UserRatingClass userrating);
    }
}
