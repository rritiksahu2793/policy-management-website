﻿using Microsoft.AspNetCore.JsonPatch;
using PolicyManagementAPI.Exception;
using PolicyManagementAPI.Model;
using PolicyManagementAPI.Repository;

namespace PolicyManagementAPI.Services
{
    public class PolicyService:IPolicyService
    {
        readonly IPolicyRepository _policyRepository;
        public PolicyService(IPolicyRepository policyRepository)
        {
            _policyRepository = policyRepository;
        }

        public bool AddPolicy(Policies policy)
        {
            Policies PolicyPresent = _policyRepository.GetPolicyById(policy.policyId);
            if (PolicyPresent != null)
            {
                throw new SomeProblemOccured("Internal Error cannot Add policy");
            }
            else
            {
                _policyRepository.AddPolicy(policy);
                return true;
            }
        }


        public bool DeletePolicy(int id)
        {
            Policies PolicyPresent = _policyRepository.GetPolicyById(id);
            if (PolicyPresent != null)
            {
                _policyRepository.DeletePolicyById(id);
                return true;
            }
            else
            {
                throw new SomeProblemOccured("Internal Error cannot Delete policy");

            }

        }

        public List<Policies> GetAllPolicy()
        {
            return _policyRepository.GetAllPolicy();
        }

        public Policies GetPolicyById(int id)
        {
            return _policyRepository.GetPolicyById(id);
        }

        public Policies GetPolicyDetails(int policyId)
        {
            return _policyRepository.GetPolicyById(policyId);
        }

        public bool UpdatePolicy(Policies policy)
        {
            if (_policyRepository.GetPolicyById(policy.policyId) == null)
            {
                throw new SomeProblemOccured("Cannot Update Policy ! Try Again ");
            }
            _policyRepository.UpdatePolicy(policy);
            return true;
        }
    }

}

