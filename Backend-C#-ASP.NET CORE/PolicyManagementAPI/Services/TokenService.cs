﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PolicyManagementAPI.Model;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace PolicyManagementAPI.Services
{
    public class TokenService:ITokenService
    {
        public string GenerateJSONWebToken(User user)
        {

            var userClaims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName,user.name)
            };
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("webtokenforpolicymanagementapp"));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "PolicyManagementApp",
              audience: "PolicyManagementUsers",
              claims: userClaims,
              expires: DateTime.Now.AddMinutes(20),
              signingCredentials: credentials);
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(token);
            return jwtSecurityTokenHandler;
            //return JsonConvert.SerializeObject(new {Token =jwtSecurityTokenHandler });
        }
    }
}
