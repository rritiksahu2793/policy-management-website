﻿using PolicyManagementAPI.Exception;
using PolicyManagementAPI.Model;
using PolicyManagementAPI.Repository;

namespace PolicyManagementAPI.Services
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        #region AddUser
        public bool AddUser(User user)
        {
            if (_userRepository.GetUserByEmail(user.email) == null)
            {
                _userRepository.AddUser(user);
                return true;
            }
            throw new UserAlreadyPresent($"User with provided {user.email} already present ! Try with another email");
        }
        #endregion

        #region DeleteUser
        public bool DeleteUser(int id)
        {
            if (_userRepository.GetUserById(id) == null)
            {
                return false;
            }
            _userRepository.DeleteUser(id);

            throw new UserNotDeletedException($"Opps Connot Delete the Object");
            
        }
        #endregion

        #region GetAllUser
        public List<User> GetAllUser()
        {
            return _userRepository.GetAllUsers();
        }
        #endregion

        # region GetUserByEmail
        public User GetUserByEmail(string email)
        {
            User user = _userRepository.GetUserByEmail(email);
            if (user!=null)
            {
                return user;
            }
            else
            {
                throw new SomeProblemOccured("Opps Some Problem Occured ! Try again");
            }
        }
        #endregion

        #region GetUserById
        public User GetUserById(int id)
        {
            return _userRepository.GetUserById(id);
        }
        #endregion

        #region GetUserPendingpolicy
        public List<UserRegisteredForPolicy> GetUserPendingPolicy(int userId)
        {
            return _userRepository.GetUserPendingPolicy(userId);
        }
        #endregion

        #region GetUserRejectedpolicy
        public List<UserRegisteredForPolicy> GetUserRejectedPolicy(int userId)
        {
            return _userRepository.GetUserRejectedPolicy(userId);
        }
        #endregion

        #region GetUserSubscribedpolicy
        public List<UserSubscribedPolicy> GetUserSubscribedPolicy(int userId)
        {
            return _userRepository.GetUserSubscribedPolicy(userId);
        }
        #endregion

        #region Login

        public User Login(LoginClass loginUser)
        {
            User user = _userRepository.Login(loginUser);
            if (user == null)
            {
                throw new UserCredentialInvalidException($"{loginUser.email} and Password did not match ! Try Again");
            }
            else
            {
                return user;
            }
        }
        #endregion

        #region PayForPolicy
        public bool PayForSubscribedPolicy(PaymentClass payForPolicy)
        {
            bool res = _userRepository.PayForSubscribedPolicy(payForPolicy);
            if (res)
            {
                return true;
            }
            else
            {
                throw new SomeProblemOccured("Opps Some Problem Occured ! Try again");
            }
        }
        #endregion

        #region RegisterPolicyForUser
        public bool RegisterPolicyForUser(UserRegisteredForPolicy userRegisteredForPolicy)
        {
            bool res = _userRepository.RegisterPolicyForUser(userRegisteredForPolicy);
            if (res)
            {
                return true;
            }
            else
            {
                throw new SomeProblemOccured("Opps Some Problem Occured ! Try again");
            }
        }
        #endregion

        #region UpdateUser
        public bool UpdateUser(User user)
        {
            if (_userRepository.GetUserById(user.id) == null)
            {
                throw new SomeProblemOccured("Cannot Update User ! Try Again ");
            }
            _userRepository.UpdateUser(user);
            return true;
        }

        public bool UserRatingForWebsite(UserRatingClass userrating)
        {
            return _userRepository.UserRatingForWebSite(userrating);
        }
        #endregion


    }
}
