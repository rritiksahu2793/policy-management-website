import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmindashboardComponent } from './components/admindashboard/admindashboard.component';
import { EdituserComponent } from './components/edituser/edituser.component';
import { LoginComponent } from './components/login/login.component';
import { PolicyDetailsComponent } from './components/policy-details/policy-details.component';
import { RegisterComponent } from './components/register/register.component';
import { UserPendingpolicyComponent } from './components/user-pendingpolicy/user-pendingpolicy.component';
import { UserRejectedpolicyComponent } from './components/user-rejectedpolicy/user-rejectedpolicy.component';
import { UserdashboardComponent } from './components/userdashboard/userdashboard.component';
import { UserdetailsComponent } from './components/userdetails/userdetails.component';
import {UserSubspolicyComponent} from './components/user-subspolicy/user-subspolicy.component';
import { Usersubscribedpolicy } from './models/usersubscribedpolicy';
import { PaymentComponent } from './components/payment/payment.component';
import { AdminviewpendingpolicyComponent } from './components/adminviewpendingpolicy/adminviewpendingpolicy.component';
import { NavComponent } from './components/nav/nav.component';
import { PolicydetailsforadminComponent } from './components/policydetailsforadmin/policydetailsforadmin.component';
import { AdmineditpolicyComponent } from './components/admineditpolicy/admineditpolicy.component';
import { DeletepolicyComponent } from './components/deletepolicy/deletepolicy.component';
import { AdminaddpolicyComponent } from './components/adminaddpolicy/adminaddpolicy.component';
import { AuthGuard } from './auth.guard';
import { AdminrejectpolicyComponent } from './components/adminrejectpolicy/adminrejectpolicy.component';
import { UserratingComponent } from './components/userrating/userrating.component';
import { AdminviewallreviewComponent } from './components/adminviewallreview/adminviewallreview.component';


const routes: Routes = [
  {path:'register',component:RegisterComponent},
  {path:'userdashboard',component:UserdashboardComponent,canActivate:[AuthGuard]},
  {path:'login',component:LoginComponent},
  {path:'policy-details/:policyId',component:PolicyDetailsComponent},
  {path:'policydetailsforadmin/:policyId',component:PolicydetailsforadminComponent},
  {path:'admineditpolicy/:policyName',component:AdmineditpolicyComponent},
  {path:'deletepolicy/:policyName',component:DeletepolicyComponent},
  {path:'userDetails',component:UserdetailsComponent},
  {path:'userpendingpolicy',component:UserPendingpolicyComponent},
  {path:'usersubspolicy',component:UserSubspolicyComponent},
  {path:'userrejectedpolicy',component:UserRejectedpolicyComponent},
  {path:'logout',component:LoginComponent},
  {path:'admindashboard',component:AdmindashboardComponent,canActivate:[AuthGuard]},
  {path:'edituser',component:EdituserComponent,canActivate:[AuthGuard]},
  {path:'payment/:policyId',component:PaymentComponent},
  {path:'adminviewpendingpolicy',component:AdminviewpendingpolicyComponent,canActivate:[AuthGuard]},
  {path:'nav',component:NavComponent},
  {path:'adminaddpolicy',component:AdminaddpolicyComponent,canActivate:[AuthGuard]},
  {path:'adminrejectpolicy/:policyId',component:AdminrejectpolicyComponent},
  {path:'userrating',component:UserratingComponent},
  {path:'adminviewallreview',component:AdminviewallreviewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
