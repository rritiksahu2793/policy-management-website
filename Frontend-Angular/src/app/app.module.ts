import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule ,HTTP_INTERCEPTORS} from  '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { UserdashboardComponent } from './components/userdashboard/userdashboard.component';
import { PolicyDetailsComponent } from './components/policy-details/policy-details.component';
import { UserdetailsComponent } from './components/userdetails/userdetails.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { UserSubspolicyComponent } from './components/user-subspolicy/user-subspolicy.component';
import { UserPendingpolicyComponent } from './components/user-pendingpolicy/user-pendingpolicy.component';
import { UserRejectedpolicyComponent } from './components/user-rejectedpolicy/user-rejectedpolicy.component';
import { LogoutComponent } from './components/logout/logout.component';
import { AdmindashboardComponent } from './components/admindashboard/admindashboard.component';
import { EdituserComponent } from './components/edituser/edituser.component';
import { PaymentComponent } from './components/payment/payment.component';
import { AdminviewpendingpolicyComponent } from './components/adminviewpendingpolicy/adminviewpendingpolicy.component';
import { AdminrejectpolicyComponent } from './components/adminrejectpolicy/adminrejectpolicy.component';
import { PolicydetailsforadminComponent } from './components/policydetailsforadmin/policydetailsforadmin.component';
import { AdmineditpolicyComponent } from './components/admineditpolicy/admineditpolicy.component';
import { DeletepolicyComponent } from './components/deletepolicy/deletepolicy.component';
import { AdminaddpolicyComponent } from './components/adminaddpolicy/adminaddpolicy.component';
import { UserratingComponent } from './components/userrating/userrating.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { AuthService } from './services/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminviewallreviewComponent } from './components/adminviewallreview/adminviewallreview.component';
import { ZReactivFormComponent } from './components/z-reactiv-form/z-reactiv-form.component';
// import Swal from 'sweetalert2';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    RegisterComponent,
    LoginComponent,
    UserdashboardComponent,
    PolicyDetailsComponent,
    UserdetailsComponent,
    UserSubspolicyComponent,
    UserPendingpolicyComponent,
    UserRejectedpolicyComponent,
    LogoutComponent,
    AdmindashboardComponent,
    EdituserComponent,
    PaymentComponent,
    AdminviewpendingpolicyComponent,
    AdminrejectpolicyComponent,
    PolicydetailsforadminComponent,
    AdmineditpolicyComponent,
    DeletepolicyComponent,
    AdminaddpolicyComponent,
    UserratingComponent,
    AdminviewallreviewComponent,
    ZReactivFormComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule ,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule
    
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
      useClass:AuthService,
      multi:true
  }],
  bootstrap: [AppComponent]
  // bootstrap: [ZReactivFormComponent]
})
export class AppModule { }
