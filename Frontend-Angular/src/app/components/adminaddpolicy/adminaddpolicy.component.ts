import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Policies } from 'src/app/models/policies';
import { PolicyService } from 'src/app/services/policy.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-adminaddpolicy',
  templateUrl: './adminaddpolicy.component.html',
  styleUrls: ['./adminaddpolicy.component.css']
})
export class AdminaddpolicyComponent {
  NewPolicy:Policies
  errors=""
  constructor(private policyServices:PolicyService){
    this.NewPolicy= new Policies()
  }
  ngOnInit():void{ 
    
  }

  AddPolicy(AddNewPolicy:NgForm){
    this.policyServices.AddPolicy(this.NewPolicy).subscribe(res=>{
      if(res){
        console.log("Policy Addded SuccessFully")
        Swal.fire(
          'Policy Added Successfully',
          'Policy Added Successfully You can see it in Dashboard',
          'success'
        )
      }
      // else{
      //   console.log("Policy Not Added")
      //   Swal.fire(
      //     'Faliure',
      //     'Registration Not Completed',
      //     'error'
      //   )
      // }
    },
    err => {
      this.errors = err.error.text;
      console.log(this.errors);
      Swal.fire(
        'Failed to add Policy',
        'Internal error failed to add policy',
        'error'
      )

    }
    )
  }

}
