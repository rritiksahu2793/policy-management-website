import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Policies } from 'src/app/models/policies';
import { PolicyService } from 'src/app/services/policy.service';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent {
  AllPolicies?: Policies[]
  userName: String
  constructor(private policyService: PolicyService, private router: Router) {
    this.userName = localStorage.getItem('userName')!
  }

  ngOnInit(): void {
    this.GetAllPolicy()
  }
  GetAllPolicy() {
    this.policyService.GetAllPolicy().subscribe(res => {
      this.AllPolicies = res
      console.log(res);

    })

  }
  myPolicies(Policy: Policies) {
    let JsonData = JSON.parse(JSON.stringify(Policy))
    localStorage.setItem('PolicyId', JsonData["policyId"]);
    localStorage.setItem('PolicyName', JsonData["policyName"]);
    this.router.navigate(['/policydetailsforadmin', +'Policy.policyId'])
    console.log(Policy);
  }
}