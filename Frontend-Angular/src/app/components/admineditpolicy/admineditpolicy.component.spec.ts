import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmineditpolicyComponent } from './admineditpolicy.component';

describe('AdmineditpolicyComponent', () => {
  let component: AdmineditpolicyComponent;
  let fixture: ComponentFixture<AdmineditpolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmineditpolicyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdmineditpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
