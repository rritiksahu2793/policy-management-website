import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Policies } from 'src/app/models/policies';
import { PolicyService } from 'src/app/services/policy.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admineditpolicy',
  templateUrl: './admineditpolicy.component.html',
  styleUrls: ['./admineditpolicy.component.css']
})
export class AdmineditpolicyComponent {
  Policies?: Policies[]
  PolicyToUpdate:Policies
  errors=""
  constructor(private policyService:PolicyService , private router:Router,private route: ActivatedRoute){
    this.PolicyToUpdate = new Policies()
  }

  ngOnInit(){
    this.policyService.GetAllPolicy().subscribe(res => {
      this.Policies = res
      const PolicyName = this.route.snapshot.paramMap.get('policyName')!
      this.Policies.forEach(element => {
        if(element.policyName == PolicyName){
          this.PolicyToUpdate=element
        }
      });
    })
  }

  EditPolicy(policy:Policies){
    this.policyService.UpdatePolicy(policy).subscribe(res=>{
      console.log(res);
      console.log("Policy updated successssfully");
      Swal.fire(
        'Poloicy Updated',
        'Policy Updated Successuflly',
        'success'
      )  
      this.router.navigate(['admindashboard'])
    },
    err => {
      this.errors = err.error.text;
      console.log(this.errors);
      Swal.fire(
        'Failed to edit Policy',
        'Internal error failed to edit policy',
        'error'
      )

    }
    )
  }


}
