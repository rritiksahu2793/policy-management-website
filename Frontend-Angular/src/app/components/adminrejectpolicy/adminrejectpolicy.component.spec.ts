import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminrejectpolicyComponent } from './adminrejectpolicy.component';

describe('AdminrejectpolicyComponent', () => {
  let component: AdminrejectpolicyComponent;
  let fixture: ComponentFixture<AdminrejectpolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminrejectpolicyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminrejectpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
