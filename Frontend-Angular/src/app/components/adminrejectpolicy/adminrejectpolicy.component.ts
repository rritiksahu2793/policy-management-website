import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Adminpolicyapprovemodel } from 'src/app/models/adminpolicyapprovemodel';
import { Policies } from 'src/app/models/policies';
import { AdminserviceService } from 'src/app/services/adminservice.service';
import { PolicyService } from 'src/app/services/policy.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-adminrejectpolicy',
  templateUrl: './adminrejectpolicy.component.html',
  styleUrls: ['./adminrejectpolicy.component.css']
})
export class AdminrejectpolicyComponent {
  ReasonForPolicyRejection: string
  UandP: Adminpolicyapprovemodel
  // feedbackinfo:Feedbackinfo
  // RejectForm :FormGroup

  errors=""
  constructor(private adminService: AdminserviceService, private router: Router, private policyService: PolicyService, private route: ActivatedRoute , private formBuilder:FormBuilder ) {
    this.UandP = new Adminpolicyapprovemodel
    this.ReasonForPolicyRejection = ""

  }
  ngOnInit() {

  }
  RejectPolicy(){
    this.UandP.policyId = +localStorage.getItem('RejectedPolicyId')!
    // this.UandP.userId = +this.route.snapshot.paramMap.get('RejectedUserId')!
    this.UandP.userId=+localStorage.getItem('RejectedUserId')!
    this.adminService.AdminRejectPolicy(this.UandP, this.ReasonForPolicyRejection).subscribe(res => {
      console.log(this.UandP);
      
      console.log("Policy Rejected Successfully");
      Swal.fire(
        'Policy Rejected ',
        'Policy Recjected Successfully',
        'success'
      )
      
    },
    err => {
      this.errors = err.error.text;
      console.log(this.errors);
      Swal.fire(
        'Failed to reject Policy',
        'Internal error failed to reject policy',
        'error'
      )

    }
    )
    this.router.navigate(['/admindashboard'])
  }


}
