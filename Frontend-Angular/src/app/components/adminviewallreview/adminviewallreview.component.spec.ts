import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminviewallreviewComponent } from './adminviewallreview.component';

describe('AdminviewallreviewComponent', () => {
  let component: AdminviewallreviewComponent;
  let fixture: ComponentFixture<AdminviewallreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminviewallreviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminviewallreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
