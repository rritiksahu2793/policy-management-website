import { Component } from '@angular/core';
import { Userrating } from 'src/app/models/userrating';
import { AdminserviceService } from 'src/app/services/adminservice.service';

@Component({
  selector: 'app-adminviewallreview',
  templateUrl: './adminviewallreview.component.html',
  styleUrls: ['./adminviewallreview.component.css']
})
export class AdminviewallreviewComponent {
  AllReview?: Userrating[]
  constructor(private adminService: AdminserviceService) {

  }
  ngOnInit() {
    this.GetAllReview()
  }

  GetAllReview() {
    this.adminService.GetAllReview().subscribe(res => {
      console.log(res);

      this.AllReview = res
    })
  }

}
