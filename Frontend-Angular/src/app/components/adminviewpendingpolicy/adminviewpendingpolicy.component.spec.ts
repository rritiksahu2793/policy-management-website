import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminviewpendingpolicyComponent } from './adminviewpendingpolicy.component';

describe('AdminviewpendingpolicyComponent', () => {
  let component: AdminviewpendingpolicyComponent;
  let fixture: ComponentFixture<AdminviewpendingpolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminviewpendingpolicyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminviewpendingpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
