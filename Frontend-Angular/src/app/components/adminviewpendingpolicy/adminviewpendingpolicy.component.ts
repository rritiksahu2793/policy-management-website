import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Adminpolicyapprovemodel } from 'src/app/models/adminpolicyapprovemodel';
import { UserAndPolicy } from 'src/app/models/user-and-policy';
import { UserRegisterForPolicy } from 'src/app/models/user-register-for-policy';
import { AdminserviceService } from 'src/app/services/adminservice.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-adminviewpendingpolicy',
  templateUrl: './adminviewpendingpolicy.component.html',
  styleUrls: ['./adminviewpendingpolicy.component.css']
})
export class AdminviewpendingpolicyComponent {
  AllPendingPolicies?: UserRegisterForPolicy[]
  UandP:Adminpolicyapprovemodel
  constructor(private adimnService: AdminserviceService , private router:Router) {  
    this.UandP= new Adminpolicyapprovemodel()
  }
  
  ngOnInit(): void {
    this.GetAllPendingPolicy()
  }
  GetAllPendingPolicy() {
    this.adimnService.GetAllPendingPolicy().subscribe(res => {
      this.AllPendingPolicies = res
      console.log(res);
      
  })

}

AdminApprovepolicy(UserApprovedpolicy:UserRegisterForPolicy){
  this.UandP.policyId = UserApprovedpolicy.policyId
  this.UandP.userId = UserApprovedpolicy.userId
  this.adimnService.AdminApprovepolicy(this.UandP).subscribe(res=>{
    console.log(res);
    Swal.fire(
      'Approved',
      'Policy Approved SuccessFully',
      'success'
    )
    
  })
  this.router.navigate(['admindashboard'])
}
AdminRejectpolicy(UserRejectedpolicy:UserRegisterForPolicy){
  localStorage.setItem('RejectedUserId', UserRejectedpolicy.userId+"");
  localStorage.setItem('RejectedPolicyId', UserRejectedpolicy.policyId+"");
  this.router.navigate(['/adminrejectpolicy', UserRejectedpolicy.policyId])
}

}
