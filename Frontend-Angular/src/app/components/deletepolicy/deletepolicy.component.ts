import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Policies } from 'src/app/models/policies';
import { PolicyService } from 'src/app/services/policy.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-deletepolicy',
  templateUrl: './deletepolicy.component.html',
  styleUrls: ['./deletepolicy.component.css']
})
export class DeletepolicyComponent {
  Policies?: Policies[]
  PolicyToDelete:Policies
  errors = ""
  constructor(private policyService:PolicyService , private router:Router,private route: ActivatedRoute){
    this.PolicyToDelete = new Policies()
  }
  ngOnInit(){
    this.policyService.GetAllPolicy().subscribe(res => {
      this.Policies = res
      const PolicyName = this.route.snapshot.paramMap.get('policyName')!
      this.Policies.forEach(element => {
        if(element.policyName == PolicyName){
          this.PolicyToDelete=element
        }
      });
    })
  }

  DeletePolicy(policy:Policies){
    this.policyService.DeletePolicy(policy.policyId+"").subscribe(res=>{
      console.log(res);
      console.log("Policy Deleted successssfully");
      Swal.fire(
        'Poloicy Deleted',
        'Policy Deleted Successuflly',
        'success'
      )  
      this.router.navigate(['admindashboard'])
    },
    err => {
      this.errors = err.error.text;
      console.log(this.errors);
      Swal.fire(
        'Failed to delete Policy',
        'Internal error failed to delete policy',
        'error'
      )

    }
    )
  }
}
