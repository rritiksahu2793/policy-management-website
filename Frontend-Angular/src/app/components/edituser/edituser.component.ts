import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserAndPolicy } from 'src/app/models/user-and-policy';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent {
  user:User
  userId:string
  uandP:UserAndPolicy
  errors=""
  constructor(private userService:UserService ,private router:Router){
    this.user=new User()
    this.uandP = new UserAndPolicy()
    this.userId = localStorage.getItem('userId')!
  }

  ngOnInit(){
    this.userService.GetUserById(this.userId).subscribe(res=>{
      this.user=res
      console.log("In user Detail component");
      console.log(this.user);
      
    })
  }
  EditUser(EditedUser:User){
    this.userService.UpdateUser(EditedUser).subscribe(res=>{
      console.log(`user updated successfully ${res}`)
      // this.uandP.UserId = this.userId
      // this.uandP.PolicyId= localStorage.getItem('PolicyId')!
      // console.log(this.uandP);
      
      // this.userService.UserRegisterForPolicy(this.uandP).subscribe(res=>{
      //   console.log(`UserRegisterforPolicy successfull ${res}`);
        
      // })
      Swal.fire(
        'Account Updated',
        'Your Account Details Updated',
        'success'
      )
      this.router.navigate(['userdashboard'])

    },
    err => {
      this.errors = err.error.text;
      console.log(this.errors);
      Swal.fire(
        'Failed to edit user',
        'Internal error failed to edit user',
        'error'
      )

    }
    )
  }
}
