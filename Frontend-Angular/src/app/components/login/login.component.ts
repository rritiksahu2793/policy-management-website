import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginUser } from 'src/app/models/login-user';

import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

// import 'sweetalert2/src/sweetalert2.scss'
// import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginUser: LoginUser
  errors = ""
  constructor(private userServices: UserService, private router: Router) {
    this.loginUser = new LoginUser()

  }
  ngOnInit(): void {
    //this.userLogin();
  }
  Login(loginForm: NgForm) {
    this.userServices.Login(this.loginUser).subscribe(res => {
     if(res){
      console.log("Login Successfull");
      // let name = JSON.stringify(res.name) 
      let JsonData = JSON.parse(JSON.stringify(res))
      console.log(JsonData);
      console.log(`JWT token ${JsonData["TOKEN"]}`);
      localStorage.setItem('userName', JsonData["Name"]);
      localStorage.setItem('userId', JsonData["Id"]);
      localStorage.setItem('token', JsonData["TOKEN"]);

      Swal.fire(
        'Login Success',
        'WelCome To LifeCare',
        'success'
      )
      if (JsonData["UserEmail"] === 'rritiksahu2793@gmail.com') {
        console.log("By admin id");
        this.router.navigate(['admindashboard'])
      }
      else {
        console.log("By user id");
        this.router.navigate(['userdashboard'])
      }
      }
    },
      err => {
        this.errors = err.error.text;
        console.log(this.errors);
        Swal.fire(
          'Login Failed',
          'User Email and Password did not match',
          'error'
        )

      }

    )
  }

}
export var name = "ritik";

