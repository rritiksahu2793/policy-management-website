import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VirtualTimeScheduler } from 'rxjs';
import { PaymentClass } from 'src/app/models/payment-class';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent {
  PayForPolicy:PaymentClass
  errors =""
 constructor(private userservice:UserService , private router:Router ,private route: ActivatedRoute){ 
   this.PayForPolicy= new PaymentClass()
 }
 ngOnInit(){
   
 }
 PayForSubscribedPolicy(PayForPolicy:PaymentClass){
    const PolicyId = this.route.snapshot.paramMap.get('policyId')!
    PayForPolicy.PolicyId = PolicyId
    PayForPolicy.UserId = localStorage.getItem('userId')!
    console.log(PayForPolicy);

    this.userservice.PayForPolicy(PayForPolicy).subscribe(res=>{

      if(res){
        console.log("Payment SuccessFull")
        Swal.fire(
          'Success',
          'Payment SuccessFull ',
          'success'
        )
        this.router.navigate(['usersubspolicy'])
      }
      // else{
      //   console.log("Payment Failed")
      //   Swal.fire(
      //     'Payment Faliure',
      //     'OPPs Some Problem Occur',
      //     'error'
      //   )
      // }

    },
    err => {
      this.errors = err.error.text;
      console.log(this.errors);
      Swal.fire(
        'Payment Failed ',
        'Internal error Payment failed ',
        'error'
      )

    }
    )
    
 }

}
