import { ThisReceiver } from '@angular/compiler';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Policies } from 'src/app/models/policies';
import { PolicyService } from 'src/app/services/policy.service';
@Component({
  selector: 'app-policy-details',
  templateUrl: './policy-details.component.html',
  styleUrls: ['./policy-details.component.css']
})
export class PolicyDetailsComponent {
  Policies?: Policies[]
  Policy: Policies
  PolicyId?:Number
  PolicyName?:string
  constructor(private policyService: PolicyService, private activatedRoute: ActivatedRoute) {
    this.Policy = new Policies
  }

  ngOnInit(): void {
    // const policyId = +this.activatedRoute.snapshot.paramMap.get('policyId')!
    // console.log(policyId);
    // this.GetPolicyDetails()
    this.GetAllPolicy()
  }

  // GetPolicyDetails() {
  //    var id = +localStorage.getItem('policyId')!
  //    this.policyService.GetPolicyDetails(id).subscribe(res => {
  //    this.Policy= res
  //    console.log(res);
  //   })
  // }

  GetAllPolicy() {
    this.policyService.GetAllPolicy().subscribe(res => {
      this.PolicyId = +localStorage.getItem('policyId')!
      this.PolicyName = localStorage.getItem('PolicyName')!
      this.Policies = res
      this.Policies.forEach(element => {
        if(element.policyName==this.PolicyName){
          this.Policy=element
        }
      });
    })
  }

}