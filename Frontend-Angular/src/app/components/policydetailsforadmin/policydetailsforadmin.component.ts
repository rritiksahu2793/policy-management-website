import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Policies } from 'src/app/models/policies';
import { PolicyService } from 'src/app/services/policy.service';

@Component({
  selector: 'app-policydetailsforadmin',
  templateUrl: './policydetailsforadmin.component.html',
  styleUrls: ['./policydetailsforadmin.component.css']
})
export class PolicydetailsforadminComponent {

  Policies?: Policies[]
  Policy: Policies
  PolicyId?:Number
  PolicyName?:string
  constructor(private policyService: PolicyService, private activatedRoute: ActivatedRoute , private router:Router) {
    this.Policy = new Policies
  }
  ngOnInit(): void {
    // const policyId = +this.activatedRoute.snapshot.paramMap.get('policyId')!
    // console.log(policyId);
    // this.GetPolicyDetails()
    this.GetAllPolicy()
  }
  GetAllPolicy() {
    this.policyService.GetAllPolicy().subscribe(res => {
      this.PolicyId = +localStorage.getItem('policyId')!
      this.PolicyName = localStorage.getItem('PolicyName')!
      this.Policies = res
      this.Policies.forEach(element => {
        if(element.policyName==this.PolicyName){
          this.Policy=element
        }
      });
    })
  }
  

}
