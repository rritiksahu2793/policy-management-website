import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  NewUser:User
  constructor(private userServices:UserService , private router:Router){
    this.NewUser= new User()
  }
  ngOnInit():void{ 
    
  }

  Register(RegisterForm:NgForm){
    this.userServices.Register(this.NewUser).subscribe(res=>{
      if(res){
        console.log("Registration SuccessFull")
        Swal.fire(
          'Success',
          'Registration SuccessFull Login to Use LifeCare',
          'success'
        )
        this.router.navigate(['login']) 
      }
      else{
        console.log("User Not Registered")
        Swal.fire(
          'Faliure',
          'Registration Not Completed',
          'error'
        )
      }
    })
  }

}
