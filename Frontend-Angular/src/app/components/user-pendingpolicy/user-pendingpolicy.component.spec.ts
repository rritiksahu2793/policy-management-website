import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPendingpolicyComponent } from './user-pendingpolicy.component';

describe('UserPendingpolicyComponent', () => {
  let component: UserPendingpolicyComponent;
  let fixture: ComponentFixture<UserPendingpolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPendingpolicyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserPendingpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
