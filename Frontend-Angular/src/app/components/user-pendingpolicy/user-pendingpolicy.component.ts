import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserRegisterForPolicy } from 'src/app/models/user-register-for-policy';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-pendingpolicy',
  templateUrl: './user-pendingpolicy.component.html',
  styleUrls: ['./user-pendingpolicy.component.css']
})
export class UserPendingpolicyComponent {
  AllPendingPolicy?: UserRegisterForPolicy[]
  userId: string
  constructor(private userService: UserService, private router: Router) {
    this.userId = localStorage.getItem('userId')!
  }
  ngOnInit(): void {
    this.GetAllPendingPolicy()
  }
  GetAllPendingPolicy() {
    this.userService.GetPendingPolicy(this.userId).subscribe(res => {
      this.AllPendingPolicy = res
      console.log(res);

    })

  }
}