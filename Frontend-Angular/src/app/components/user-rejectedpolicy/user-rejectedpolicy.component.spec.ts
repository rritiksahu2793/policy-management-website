import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRejectedpolicyComponent } from './user-rejectedpolicy.component';

describe('UserRejectedpolicyComponent', () => {
  let component: UserRejectedpolicyComponent;
  let fixture: ComponentFixture<UserRejectedpolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserRejectedpolicyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserRejectedpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
