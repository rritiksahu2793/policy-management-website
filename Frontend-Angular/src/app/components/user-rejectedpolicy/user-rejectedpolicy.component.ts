import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserRegisterForPolicy } from 'src/app/models/user-register-for-policy';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-rejectedpolicy',
  templateUrl: './user-rejectedpolicy.component.html',
  styleUrls: ['./user-rejectedpolicy.component.css']
})
export class UserRejectedpolicyComponent {
  AllRejectedPolicy?: UserRegisterForPolicy[]
  userId: string
  constructor(private userService: UserService, private router: Router) {
    this.userId = localStorage.getItem('userId')!
  }
  ngOnInit(): void {
    this.GetRejectedPolicy()
  }
  GetRejectedPolicy() {
    this.userService.GetRejectedPolicy(this.userId).subscribe(res => {
      this.AllRejectedPolicy = res
      console.log(res);

    })

  }

}
