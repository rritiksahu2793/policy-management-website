import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubspolicyComponent } from './user-subspolicy.component';

describe('UserSubspolicyComponent', () => {
  let component: UserSubspolicyComponent;
  let fixture: ComponentFixture<UserSubspolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserSubspolicyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserSubspolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
