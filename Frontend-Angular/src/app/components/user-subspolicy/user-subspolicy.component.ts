import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Usersubscribedpolicy } from 'src/app/models/usersubscribedpolicy';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-subspolicy',
  templateUrl: './user-subspolicy.component.html',
  styleUrls: ['./user-subspolicy.component.css']
})
export class UserSubspolicyComponent {
  AllSubscribedPolicy?: Usersubscribedpolicy[]
  userId: string
  constructor(private userService: UserService, private router: Router) {
    this.userId = localStorage.getItem('userId')!
  }
  ngOnInit(): void {
    this.GetSubscribedPolicy()
  }
  GetSubscribedPolicy() {
    this.userService.GetSubscribedPolicy(this.userId).subscribe(res => {
      this.AllSubscribedPolicy = res
      console.log(res);

    })

  }

}
