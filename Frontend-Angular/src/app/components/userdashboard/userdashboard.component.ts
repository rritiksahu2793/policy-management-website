import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Policies } from 'src/app/models/policies';
import { PolicyService } from 'src/app/services/policy.service';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent {
  AllPolicies?: Policies[]
  userName:String

  // image: string[] = ["https://media.istockphoto.com/id/1353453774/photo/close-up-of-woman-signing-medical-agreement-at-doctors-office.jpg?b=1&s=170667a&w=0&k=20&c=QkynReEUynYBBJ6y3nwzt8HX7JHOA8uJNgPYRbJKe8g=",
  //   "https://media.istockphoto.com/id/1304320125/photo/babys-hand-with-heart-object-and-mothers-hand.jpg?b=1&s=170667a&w=0&k=20&c=JV0Flm3s0giQrXKhw7-dxGOXpUqKZ7JmbB_ecbp7O1M=",
  //   "https://cdn.pixabay.com/photo/2022/11/30/17/04/car-7627218__340.jpg",
  //   "https://cdn.pixabay.com/photo/2017/10/16/19/03/arson-2858155__340.jpg",
  //   "https://media.istockphoto.com/id/1173389403/photo/portrait-of-laughing-senior-couple.jpg?b=1&s=170667a&w=0&k=20&c=5l2CJgtYdutbznLmzY8HtpS0LsCx9RJZzF44q-Djr5s="
  // ]


  constructor(private policyService: PolicyService , private router:Router) {
    this.userName=localStorage.getItem('userName')!
  }

  ngOnInit(): void {
    this.GetAllPolicy()
  }
  GetAllPolicy() {
    this.policyService.GetAllPolicy().subscribe(res => {
      this.AllPolicies = res
      console.log(res);
      
  })

  }
  myPolicies(Policy:Policies){
      let JsonData = JSON.parse(JSON.stringify(Policy))
      localStorage.setItem('PolicyId',JsonData["policyId"]);
      localStorage.setItem('PolicyName',JsonData["policyName"]);
      this.router.navigate(['/policy-details',+'Policy.policyId'])
      console.log(Policy);  
  }

}
