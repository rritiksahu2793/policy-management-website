import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserAndPolicy } from 'src/app/models/user-and-policy';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.css']
})
export class UserdetailsComponent {
  user:User
  userId:string
  uandP:UserAndPolicy
  constructor(private userService:UserService ,private router:Router){
    this.user=new User()
    this.uandP = new UserAndPolicy()
    this.userId = localStorage.getItem('userId')!
  }

  ngOnInit(){
    this.userService.GetUserById(this.userId).subscribe(res=>{
      this.user=res
      console.log("In user Detail component");
      console.log(this.user);
      
    })
  }
  EditUserAndSubsPolicy(EditedUser:User){
    this.userService.UpdateUser(EditedUser).subscribe(res=>{
      console.log(`user updated successfully ${res}`)
      this.uandP.UserId = this.userId
      this.uandP.PolicyId= localStorage.getItem('PolicyId')!
      console.log(this.uandP);
      
      this.userService.UserRegisterForPolicy(this.uandP).subscribe(res=>{
        console.log(`UserRegisterforPolicy successfull ${res}`);
        
      })
      Swal.fire(
        'Policy Subscription Successfull',
        'NOTE : Your Policy status will we pending Now . Once we verify your details your Policy will we Approved. You can always check your policy status on Your account',
        'success'
      )
      this.router.navigate(['userdashboard'])

    })
  }
  

}
