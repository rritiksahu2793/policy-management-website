import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Userrating } from 'src/app/models/userrating';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-userrating',
  templateUrl: './userrating.component.html',
  styleUrls: ['./userrating.component.css']
})
export class UserratingComponent {
  userRating:Userrating
  comment?:string
  Rating?:Number
  color1?: string
  color2?: string
  color3?: string
  color4?: string
  color5?: string

  constructor(private userService:UserService, private router:Router) { 
    this.color1="enabled",this.color2="enabled",this.color3="enabled",this.color4="enabled",this.color5="enabled"
    this.userRating = new Userrating()
    this.comment=""
  }
  Rate1() {
    this.color1 = "warn"
    this.Rating=1
  }
  Rate2() {
    this.color1 = "warn", this.color2 = "warn"
    this.Rating=2
  }
  Rate3() {
    this.color1 = "warn", this.color2 = "warn", this.color3 = "warn"
    this.Rating=3
  }
  Rate4() {
    this.color1 = "warn", this.color2 = "warn", this.color3 = "warn", this.color4 = "warn"
    this.Rating=4
  }
  Rate5() {
    this.color1 = "warn", this.color2 = "warn", this.color3 = "warn", this.color4 = "warn", this.color5 = "warn"
    this.Rating=5
  }

  Summit(){
    this.userRating.comment=this.comment
    this.userRating .rating=this.Rating
    this.userRating .userName= localStorage.getItem('userName')!
    this.userService.UserRatingForWebSite(this.userRating).subscribe(res=>{
      console.log(res);
      this.router.navigate(['login'])
      
    })

  }

}
