import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZReactivFormComponent } from './z-reactiv-form.component';

describe('ZReactivFormComponent', () => {
  let component: ZReactivFormComponent;
  let fixture: ComponentFixture<ZReactivFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZReactivFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZReactivFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
