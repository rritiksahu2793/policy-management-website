import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginUser } from 'src/app/models/login-user';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-z-reactiv-form',
  templateUrl: './z-reactiv-form.component.html',
  styleUrls: ['./z-reactiv-form.component.css']
})
export class ZReactivFormComponent {

  LoginUser:LoginUser
  LoginForm:FormGroup
  constructor(private formBuilder:FormBuilder,private router:Router){
    this.LoginUser=new LoginUser();
    this.LoginForm=formBuilder.group({
      
    })
  }
  
  submitRating(feedbackForm:FormGroup){
    this.LoginUser.email=this.LoginForm.value.email
    this.LoginUser.password=this.LoginForm.value.password
    console.log(LoginUser);
    Swal.fire(
      'Thank you!',
      'Your feedback has been submitted successfully',
      'success'
    )

  }

}
