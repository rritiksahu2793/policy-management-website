export class UserRegisterForPolicy {
    id?:number
    userId?:number
    userEmail?:string
    userName?:string
    policyId?:number
    policyName?:string
    status?:string
    reasonForCancellation?:string
    
}
