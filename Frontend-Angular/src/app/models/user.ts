export class User {
    id?:number
    name?:string
    password?:string
    city?:string
    email?:string
    age?:number
    aadharNo?:number
    panNo?:string
    salary?:number
    state?:string
    mobile?:number
}
