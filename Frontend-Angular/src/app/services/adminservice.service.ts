import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Adminpolicyapprovemodel } from '../models/adminpolicyapprovemodel';
import { UserAndPolicy } from '../models/user-and-policy';
import { UserRegisterForPolicy } from '../models/user-register-for-policy';
import { Userrating } from '../models/userrating';

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {

  constructor(private httpclient:HttpClient) { }

   GetAllPendingPolicy():Observable<UserRegisterForPolicy[]>{
    return this.httpclient.get<UserRegisterForPolicy[]>('https://localhost:7276/api/Admin/GetAllPendingPolicy');
   }

   AdminApprovepolicy(UandP:Adminpolicyapprovemodel):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/Admin/AdminApprovePolicy',UandP)
   }

   AdminRejectPolicy(UandP:Adminpolicyapprovemodel,reasonForRejection:string):Observable<boolean>{
    return this.httpclient.post<boolean>(`https://localhost:7276/api/Admin/AdminRejectPolicy?reasonForCancellation=${reasonForRejection}`,UandP)
   }

   GetAllReview():Observable<Userrating[]>{
    return this.httpclient.get<Userrating[]>('https://localhost:7276/api/Admin/GetAllReviews');
   }

}


