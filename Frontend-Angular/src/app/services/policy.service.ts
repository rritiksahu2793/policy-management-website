import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Policies } from '../models/policies';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PolicyService {

  constructor(private httpclient:HttpClient) { }

  GetAllPolicy():Observable<Policies[]>{
    return this.httpclient.get<Policies[]>('https://localhost:7276/api/Policy');
  }

  AddPolicy(NewPolicy:Policies):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/Policy/AddPolicy',NewPolicy);
  }

  GetPolicyById(Id:String):Observable<Policies>{
    return this.httpclient.get<Policies>(`https://localhost:7276/api/Policy/GetPolicyDetails/${Id}`);
  }

  UpdatePolicy(policy:Policies):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/Policy/UpdatePolicy'+policy.policyId, policy);
  }

  DeletePolicy(PolicyId:string):Observable<boolean>{
    return this.httpclient.delete<boolean>('https://localhost:7276/api/Policy/DeletePolicy/'+PolicyId);
  }

}
