import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginUser } from '../models/login-user';
import { PaymentClass } from '../models/payment-class';
import { User } from '../models/user';
import { UserAndPolicy } from '../models/user-and-policy';
import { UserRegisterForPolicy } from '../models/user-register-for-policy';
import { Userrating } from '../models/userrating';
import { Usersubscribedpolicy } from '../models/usersubscribedpolicy';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpclient:HttpClient) { }
  Login(loginUser:LoginUser):Observable<User>{
    return this.httpclient.post<User>('https://localhost:7276/api/User/LoginUser',loginUser);
  }

  Register(NewUser:User):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/User/AddUser',NewUser);
  }

  GetUserById(Id:String):Observable<User>{
    return this.httpclient.get<User>(`https://localhost:7276/api/User/GetUserById/${Id}`);
  }

  UpdateUser(user:User):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/User/UpdateUser'+user.id,user)
  }

  UserRegisterForPolicy(UandP:UserAndPolicy):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/User/UserRegisterForPolicy',UandP)
  }

  GetSubscribedPolicy(userId:string):Observable<Usersubscribedpolicy[]>{
    return this.httpclient.get<Usersubscribedpolicy[]>('https://localhost:7276/api/User/GetUserSubscribedPolicy/'+userId);
  }

  GetPendingPolicy(userId:string):Observable<UserRegisterForPolicy[]>{
    return this.httpclient.get<UserRegisterForPolicy[]>('https://localhost:7276/api/User/GetUserPendingPolicy/'+userId);
  }

  GetRejectedPolicy(userId:string):Observable<UserRegisterForPolicy[]>{
    return this.httpclient.get<UserRegisterForPolicy[]>('https://localhost:7276/api/User/GetUserRejectedPolicy/'+userId);
  }

  PayForPolicy(PayForPolicy:PaymentClass):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/User/PayForPolicy',PayForPolicy)
  }

  UserRatingForWebSite(userrating:Userrating):Observable<boolean>{
    return this.httpclient.post<boolean>('https://localhost:7276/api/User/UserRateforWebSite',userrating)
  }

}
